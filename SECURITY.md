# Security Policy

This project implements the Eclipse Foundation Security Policy

* https://www.eclipse.org/security

## Supported Versions

These versions of Eclipse Titan are currently being supported with security
updates.

| Version | Released   | Supported          | 
| ------- | ---------- | ------------------ | 
| 9.0.0   | 2023-05-17 | :white_check_mark: | 
| 8.3.0   | 2023-01-17 | :white_check_mark: | 
| 8.2.0   | 2022-05-09 | :white_check_mark: | 
| < 8.1.0 | 2021-12-15 | :x:                | 

## Reporting a Vulnerability

Please report vulnerabilities to the Eclipse Foundation Security Team at
security@eclipse.org
