/******************************************************************************
* Copyright (c) 2000-2023 Ericsson Telecom AB
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
*
* Contributors:
*   Baranyi, Botond
*
******************************************************************************/

module sensitive_data {

type record UserData {
  charstring    username,
  octetstring  secret_key
} with {
  extension (secret_key) "sensitive_data"
}

type octetstring OPc with { extension "sensitive_data" }

type record of integer IntList
with {
  extension ([-]) "sensitive_data";
}

type set of integer IntSet
with {
  extension ([-]) "sensitive_data";
}

type port PT_msg message {
  inout OPc
}
with { extension "internal" }

signature Sig(inout OPc x) return OPc exception (OPc);

type port PT_proc procedure {
  inout Sig
}
with { extension "internal" }

type integer address with { extension "sensitive_data" }

type port PT_msg_addr message {
  inout integer
}
with { extension "address" }

type port PT_proc_addr procedure {
  inout Sig
}
with { extension "address" }

type component CT {
  port PT_msg pt_msg;
  port PT_proc pt_proc;
}

// todo: add some outside test for sensitive data in the log file
// (Currently these ports are mostly only here as a compilation test.
//  Only 'log2str' and 'ttcn2string' can be tested in TTCN-3.)

function test(boolean redacted) runs on CT {
  var UserData x1 := { username := "User1", secret_key := '11223344'O }
  var OPc x2 := 'DEADBEEF'O;
  var IntList x3 := { 1, 2, 4 };
  var anytype x4 := { OPc := 'FE'O };
  var IntSet x5 := { 4, 5 };
  log(x1);
  log(x2);
  log(x3);
  log(x4);
  log(x5);
  
  var template UserData t1 := { username := "User1", secret_key := '112233FF'O }
  var template OPc t2 := 'DEADBEEF22'O;
  var template IntList t3 := { 1, 2, 6 };
  var template anytype t4 := { OPc := 'BA'O };
  var template IntSet t5 := { 4, 1 };
  var template IntSet t5s := subset(2, 3, 4);
  log(t1);
  log(t2);
  log(t3);
  log(t4);
  log(t5);
  log(t5s);
  log(match(x1, t1));
  log(match(x2, t2));
  log(match(x3, t3));
  log(match(x4, t4));
  log(match(x5, t5));
  log(match(x5, t5s));
  
  connect(self:pt_msg, self:pt_msg);
  connect(self:pt_proc, self:pt_proc);
  
  pt_msg.send('ABCD'O);
  var OPc msg;
  timer t := 1.0;
  t.start;
  alt {
    [] pt_msg.receive(OPc: ?) -> value msg { t.stop; log(msg); }
    [] t.timeout { setverdict(fail, "receive timed out"); }
  }
  
  var OPc call_par;
  pt_proc.call(Sig: { '12EF'O }, nowait);
  t.start;
  alt {
    [] pt_proc.getcall(Sig: { ? }) -> param (call_par) { t.stop; log(call_par); }
    [] t.timeout { setverdict(fail, "getcall timed out"); }
  }
  
  var OPc reply_par;
  var OPc ret_val;
  pt_proc.reply(Sig: { '34CD'O } value '56AB'O);
  t.start;
  alt {
    [] pt_proc.getreply(Sig: { ? } value OPc: ?) -> value ret_val param (reply_par) {
      t.stop; log(reply_par); log(ret_val); }
    [] t.timeout { setverdict(fail, "getreply timed out"); }
  }
  
  var OPc exc;
  pt_proc.raise(Sig, 'FFFF'O);
  t.start;
  alt {
    [] pt_proc.catch(Sig, OPc: ?) -> value exc { t.stop; log(exc); }
    [] t.timeout { setverdict(fail, "catch timed out"); }
  }
  
  disconnect(self:pt_msg, self:pt_msg);
  disconnect(self:pt_proc, self:pt_proc);
  
  var charstring x1_log, x2_log, x3_log, x4_log, x5_log,
    t1_log, t2_log, t3_log, t4_log, t5_log, t5s_log,
    t1_log_match, t2_log_match, t3_log_match, t4_log_match, t5_log_match, t5s_log_match,
    msg_log, call_par_log, reply_par_log, ret_val_log, exc_log, x1_ttcn2string;
    
  if (redacted) {
    x1_log := "{ username := \"User1\", secret_key := <redacted> }";
    x2_log := "<redacted>";
    x3_log := "{ <redacted>, <redacted>, <redacted> }";
    x4_log := "{ OPc := <redacted> }";
    x5_log := "{ <redacted>, <redacted> }";
    t1_log := "{ username := \"User1\", secret_key := <redacted> }";
    t2_log := "<redacted>";
    t3_log := "{ <redacted>, <redacted>, <redacted> }";
    t4_log := "{ OPc := <redacted> }";
    t5_log := "{ <redacted>, <redacted> }";
    t5s_log := "subset(<redacted>, <redacted>, <redacted>)";
    t1_log_match := "<redacted>";
    t2_log_match := "<redacted>";
    t3_log_match := "<redacted>";
    t4_log_match := "<redacted>";
    t5_log_match := "<redacted>";
    t5s_log_match := "{ <redacted>, <redacted> } with subset(<redacted>, <redacted>, <redacted>) unmatched";
    msg_log := "<redacted>";
    call_par_log := "<redacted>";
    reply_par_log := "<redacted>";
    ret_val_log := "<redacted>";
    exc_log := "<redacted>";
  }
  else {
    x1_log := "{ username := \"User1\", secret_key := '11223344'O }";
    x2_log := "'DEADBEEF'O";
    x3_log := "{ 1, 2, 4 }";
    x4_log := "{ OPc := 'FE'O }";
    x5_log := "{ 4, 5 }";
    t1_log := "{ username := \"User1\", secret_key := '112233FF'O }";
    t2_log := "'DEADBEEF22'O";
    t3_log := "{ 1, 2, 6 }";
    t4_log := "{ OPc := 'BA'O }";
    t5_log := "{ 4, 1 }";
    t5s_log := "subset(2, 3, 4)";
    t1_log_match := ".secret_key := '11223344'O with '112233FF'O unmatched";
    t2_log_match := "'DEADBEEF'O with 'DEADBEEF22'O unmatched";
    t3_log_match := "[2] := 4 with 6 unmatched";
    t4_log_match := ".OPc := 'FE'O with 'BA'O unmatched";
    t5_log_match := "[1 <-> 1] := 5 with 1 unmatched";
    t5s_log_match := "{ 4, 5 } with subset(2, 3, 4) unmatched";
    msg_log := "'ABCD'O";
    call_par_log := "'12EF'O";
    reply_par_log := "'34CD'O";
    ret_val_log := "'56AB'O";
    exc_log := "'FFFF'O";
  }
  x1_ttcn2string := "{ username := \"User1\", secret_key := '11223344'O }";
  
  if (log2str(x1) != x1_log) {
    setverdict(fail, "x1: ", x1);
  }
  if (log2str(x2) != x2_log) {
    setverdict(fail, "x2: ", x2);
  }
  if (log2str(x3) != x3_log) {
    setverdict(fail, "x3: ", x3);
  }
  if (log2str(x4) != x4_log) {
    setverdict(fail, "x4: ", x4);
  }
  if (log2str(x5) != x5_log) {
    setverdict(fail, "x5: ", x5);
  }
  if (log2str(t1) != t1_log) {
    setverdict(fail, "t1: ", t1);
  }
  if (log2str(t2) != t2_log) {
    setverdict(fail, "t2: ", t2);
  }
  if (log2str(t3) != t3_log) {
    setverdict(fail, "t3: ", t3);
  }
  if (log2str(t4) != t4_log) {
    setverdict(fail, "t4: ", t4);
  }
  if (log2str(t5) != t5_log) {
    setverdict(fail, "t5: ", t5);
  }
  if (log2str(t5s) != t5s_log) {
    setverdict(fail, "t5s: ", t5s);
  }
  if (log2str(match(x1, t1)) != t1_log_match) {
    setverdict(fail, "match(x1, t1): ", match(x1, t1));
  }
  if (log2str(match(x2, t2)) != t2_log_match) {
    setverdict(fail, "match(x2, t2): ", match(x2, t2));
  }
  if (log2str(match(x3, t3)) != t3_log_match) {
    setverdict(fail, "match(x3, t3): ", match(x3, t3));
  }
  if (log2str(match(x4, t4)) != t4_log_match) {
    setverdict(fail, "match(x4, t4): ", match(x4, t4));
  }
  if (log2str(match(x5, t5)) != t5_log_match) {
    setverdict(fail, "match(x5, t5): ", match(x5, t5));
  }
  if (log2str(match(x5, t5s)) != t5s_log_match) {
    setverdict(fail, "match(x5, t5s): ", match(x5, t5s));
  }
  if (log2str(msg) != msg_log) {
    setverdict(fail, "msg: ", msg);
  }
  if (log2str(call_par) != call_par_log) {
    setverdict(fail, "call_par: ", call_par);
  }
  if (log2str(reply_par) != reply_par_log) {
    setverdict(fail, "reply_par: ", reply_par);
  }
  if (log2str(ret_val) != ret_val_log) {
    setverdict(fail, "ret_val: ", ret_val);
  }
  if (log2str(exc) != exc_log) {
    setverdict(fail, "exc: ", exc);
  }
  if (ttcn2string(x1) != x1_ttcn2string) {
    setverdict(fail, "ttcn2string: ", ttcn2string(x1));
  }
  setverdict(pass);
}

testcase tc_sensitive_data_displayed() runs on CT {
  test(false);
}

testcase tc_sensitive_data_redacted() runs on CT {
  test(true);
}

}
with {
  extension "anytype OPc";
}
